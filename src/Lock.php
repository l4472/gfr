<?php

namespace Logymetria\Gfr;

class Lock
{
    public int $id;
    public int $serial;
    public int $maxPorts;

    public function __construct(int $serial, int $id = 0, int $maxPorts = 31)
    {
        $this->id = $id;
        $this->serial = $serial;
        $this->maxPorts = $maxPorts;
    }
}