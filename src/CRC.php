<?php

namespace Logymetria\Gfr;

class CRC
{
    public static function CRC16($data): int
    {
        $dadoArray = array_reverse(unpack("C*", pack("L", $data)));

        $length = 4;

        return self::crc16part($dadoArray, $length);
    }

    public static function crc16part($dadoArray, $length): int
    {
        $crc = 0xFFFF;
        $i = 0;

        while ($length--) {
            $x = (($crc >> 8) ^ $dadoArray[$i++]) & 0xFF;
            $x ^= ($x >> 4);
            $crc = (($crc << 8) ^ (($x << 12)) ^ (($x << 5)) ^ ($x)) & 0xFFFF;
        }

        return $crc;
    }

    public static function CRC4($data, $size): int // CRC-4-ITU
    {
        $r = 0;

        for ($i = 0; $i < $size; $i++) {
            $r ^= $data[$i];

            $t = (~(($r & 1) - 1));
            $r = (($r >> 1) ^ (0xC & $t));

            $t = (~(($r & 1) - 1));
            $r = (($r >> 1) ^ (0xC & $t));

            $t = (~(($r & 1) - 1));
            $r = (($r >> 1) ^ (0xC & $t));

            $t = (~(($r & 1) - 1));
            $r = (($r >> 1) ^ (0xC & $t));

            $t = (~(($r & 1) - 1));
            $r = (($r >> 1) ^ (0xC & $t));

            $t = (~(($r & 1) - 1));
            $r = (($r >> 1) ^ (0xC & $t));

            $t = (~(($r & 1) - 1));
            $r = (($r >> 1) ^ (0xC & $t));

            $t = (~(($r & 1) - 1));
            $r = (($r >> 1) ^ (0xC & $t));

        }

        return $r;
    }
}