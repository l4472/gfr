<?php

namespace Logymetria\Gfr;

use Exception;

class Process
{
    public Lock $lock;

    public Operator $operator;

    public int $port;

    public int $clientKey;

    public int $randomPassword;

    public int $count = 0;

    public function __construct(Lock $lock, $clientKey)
    {
        $this->lock = $lock;
        $this->clientKey = $clientKey;
    }

    public function openingCode(int $randomPassword): int
    {
        $decryptRandomPassword = self::explodePassword($randomPassword, $this->lock);

        $this->port = $decryptRandomPassword['port'];
        $this->count = $decryptRandomPassword['count'];

        return Crypto::encrypt($this->count + ($this->port << 10), $this->clientKey + ($this->lock->id << 16));
    }

    public function closingCode(int $openingCode): int
    {
        $key = $this->clientKey + ($this->lock->id << 16);

        $openingCode = Crypto::decrypt($openingCode, $key);

        $data = [];
        for ($i = 0; $i < 12; $i++) {
            if ($i === 0) {
                $array = Util::ToByteArray32($openingCode);
                $data += $array;
                $i += count($array);
            }

            if ($i === 4) {
                $array = Util::ToByteArray16($this->lock->id, 4);
                $data += $array;
                $i += count($array);
            }

            $data[] = 0;
        }

        return CRC::crc16part($data, 6);
    }

    public function generateAttachCode(): string
    {
        $customKey = $this->clientKey + ($this->lock->id << 16);

        return str_pad(Crypto::encrypt($customKey, $this->lock->serial), 10, 0, STR_PAD_LEFT) . str_pad(CRC::CRC16($customKey), 5, 0, STR_PAD_LEFT);
    }

    public function generateDetachCode(string $detachPassword): ?string
    {
        $data = Crypto::decrypt($detachPassword, $this->lock->id);

        $count = (($data >> 14) & 0x3FF);

        if (($data & 0x3F) !== 0) {
            return null;
        }

        $code = ($this->lock->id << 16) + ($count << 6) + (0 & 0x3F);

        $customKey = $this->clientKey + ($this->lock->id << 16);

        return Crypto::encrypt($code, $customKey);
    }

    private function generatePassword(Operator $operator, int $port): int
    {
        $this->operator = $operator;
        $this->port = $port;

        $password = $operator->prefix . $operator->append;

        $randomPassword = Crypto::encrypt((((($password << 10) + $this->count) << 5) + $this->port), $this->lock->id);

        ++$this->count;

        return $this->randomPassword = $randomPassword;
    }

    public static function explodePassword(int $password, Lock $lock): array
    {
        $decryptRandomPassword = Crypto::decrypt($password, $lock->id);

        $randomPassword = $decryptRandomPassword >> 15;

        $prefix = substr($randomPassword, 0, 4);
        $append = substr($randomPassword, -1, 1);

        $port = ($decryptRandomPassword & 0x1F);
        $count = (($decryptRandomPassword >> 5) & 0x3FF);

        return [
            'prefix' => $prefix,
            'append' => $append,
            'port' => $port,
            'count' => $count,
        ];
    }

    /**
     * @throws Exception
     */
    public function checkPassword(): bool
    {
        $password = $this->randomPassword >> 15;

        $prefix = substr($password, 0, 4);
        $append = substr($password, -1, 1);

        $port = ($password & 0x1F);
        $count = (($password >> 5) & 0x3FF);

        return (Operator::generatePassword($prefix, $append) === $this->operator->password || Operator::generatePassword($prefix, 9) === $this->operator->password) && $port === $this->port && $count === $this->count;
    }
}