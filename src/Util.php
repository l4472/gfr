<?php

namespace Logymetria\Gfr;

class Util
{
    public static function ToByteArray32($dado, int $index = 0): array
    {
        return [
            $index => $dado >> 24,
            ++$index => $dado >> 16,
            ++$index => $dado >> 8,
            ++$index => $dado,
        ];
    }

    public static function ToByteArray16($dado, int $index = 0): array
    {
        return [
            $index => $dado >> 8,
            ++$index => $dado,
        ];
    }
}