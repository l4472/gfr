<?php

namespace Logymetria\Gfr;

use Exception;
use InvalidArgumentException;

class Operator
{
    public int $prefix;
    public int $suffix;
    public int $code;
    public int $append;
    public int $password;

    public bool $coercion;

    /**
     * @throws Exception
     */
    public function __construct($password)
    {
        if (strlen($password) !== 6) {
            throw new InvalidArgumentException('Password must have 6 digits');
        }

        $this->password = $password;
        $this->prefix = substr($this->password, 0, 4);
        $this->append = substr($this->password, -1, 1);
        $this->code = substr($this->password, -2, 1);
        $this->suffix = $this->code . $this->append;
        $this->coercion = $this->append === 9;
    }

    /**
     * @throws Exception
     */
    public static function generatePassword(int $prefix, ?int $append = null): int
    {
        if (strlen($prefix) !== 4) {
            throw new InvalidArgumentException('Password must have 4 digits');
        }

        if ($append !== null && strlen($append) !== 1) {
            throw new InvalidArgumentException('Password must have 1 digits');
        }

        $append = $append ?? random_int(0, 8);

        $code = self::generateCode($prefix, $append);

        return $prefix . $code . $append;
    }

    public function checkPassword(int $prefix, int $append): bool
    {
        $validatorCode = self::generateCode($prefix, $append);

        return $validatorCode === $this->code;
    }

    private static function generateCode(int $prefix, int $append): int
    {
        $password = $prefix . $append;

        $dados = [
            (($password >> 16) & 0xFF),
            (($password >> 8) & 0xFF),
            ($password & 0xFF)
        ];

        return CRC::CRC4($dados, 3) % 10;
    }
}