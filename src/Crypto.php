<?php

namespace Logymetria\Gfr;

class Crypto
{
    public Lock $lock;
    public int $key;

    public const tbPC1 = [16, 22, 30, 12, 31, 32, 23, 25, 14, 18, 20, 2, 15, 24, 11, 10, 27, 19, 4, 6, 28, 8, 7, 3];

    public const tbInitialPermutation = [29, 18, 10, 2, 20, 12, 32, 4, 22, 14, 31, 6, 24, 25, 16, 8, 17, 30, 26, 9, 1, 19, 11, 3, 21, 13, 27, 5, 23, 28, 15, 7];

    public const tbPC2 = [23, 14, 17, 22, 11, 1, 5, 3, 20, 21, 15, 6, 10, 12, 4, 19, 8, 16, 7, 24, 13, 2, 9, 18];

    public const tbExpansion = [12, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9, 8, 9, 10, 11, 12, 13, 12, 13, 14, 15, 16, 1];

    public const tbPermutation = [7, 12, 1, 5, 13, 14, 10, 2, 8, 3, 15, 9, 6, 16, 11, 4];

    public const rotR = [1, 2, 2, 1, 2, 1, 1, 2, 2, 2, 1, 1, 2, 1, 1, 1];

    public const rotL = [1, 1, 2, 2, 2, 2, 1, 1, 2, 1, 2, 1, 1, 1, 2, 2];

    public const tbSBox0 = [
        [5, 10, 2, 6, 15, 9, 0, 14, 4, 13, 7, 1, 11, 3, 8, 12,],
        [4, 0, 12, 9, 1, 5, 8, 10, 3, 15, 7, 11, 13, 2, 6, 14],
        [6, 10, 3, 5, 9, 0, 1, 11, 8, 12, 13, 4, 2, 14, 15, 7,],
        [0, 6, 11, 2, 12, 9, 10, 15, 1, 4, 7, 13, 5, 3, 14, 8,],
    ];

    public const tbSBox1 = [
        [14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7],
        [0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8],
        [4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0],
        [15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13],
    ];

    public const tbSBox2 = [
        [15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10],
        [3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5],
        [0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15],
        [13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9]
    ];

    public const tbSBox3 = [
        [10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8],
        [13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1],
        [13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7],
        [1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12]
    ];

    public function __construct(Lock $lock, $key)
    {
        $this->lock = $lock;
        $this->key = $key;
    }

    public static function encrypt(int $dado, int $key): int
    {
        //IP
        $dado = self::permutation($dado, self::tbInitialPermutation, 32);

        $dadoBaixo = $dado & 0xFFFF;
        $dadoAlto = ($dado >> 16) & 0xFFFF;

        $key = self::permutation($key, self::tbPC1, 24);

        /**********************************************************/
        for ($i = 0; $i < 16; $i++) {
            /******************** Função Feistel adapitada *******************************/
            $dadoAux = $dadoBaixo;

            //expansão
            $dadoBaixo = self::permutation($dadoBaixo, self::tbExpansion, 24);

            //xor
            $dadoBaixo ^= self::generateSubKey($key, $i);

            //S-BOX
            $dadoBaixo = self::sBox($dadoBaixo);

            $dadoBaixo = self::permutation($dadoBaixo, self::tbPermutation, 16);
            /**************************************************************************************/
            if ($i === 15) {
                $dadoAlto ^= $dadoBaixo;
                $dadoBaixo = $dadoAux;
            } else {
                $dadoBaixo ^= $dadoAlto;
                $dadoAlto = $dadoAux;
            }
        }

        //FP
        $dado = $dadoBaixo + ($dadoAlto << 16);

        return self::reversePermutation($dado, self::tbInitialPermutation, 32);
    }

    public static function decrypt(int $dado, int $key): int
    {
        //IP
        $dado = self::permutation($dado, self::tbInitialPermutation, 32);

        $dadoBaixo = $dado & 0xFFFF;
        $dadoAlto = ($dado >> 16) & 0xFFFF;

        $key = self::permutation($key, self::tbPC1, 24);

        /**********************************************************/
        for ($i = 15; $i >= 0; $i--) {
            /******************** Função Feistel para 16 bits *******************************/
            $dadoAux = $dadoBaixo;

            //expansão
            $dadoBaixo = self::permutation($dadoBaixo, self::tbExpansion, 24);

            //xor
            $dadoBaixo ^= self::generateSubKey($key, $i);

            //S-BOX
            $dadoBaixo = self::sBox($dadoBaixo);

            $dadoBaixo = self::permutation($dadoBaixo, self::tbPermutation, 16);
            /***************************************************************************/
            if ($i === 0) {
                $dadoAlto ^= $dadoBaixo;
                $dadoBaixo = $dadoAux;
            } else {
                $dadoBaixo ^= $dadoAlto;
                $dadoAlto = $dadoAux;
            }

        }

        //FP
        $dado = $dadoBaixo + ($dadoAlto << 16);

        return self::reversePermutation($dado, self::tbInitialPermutation, 32);
    }

    private static function permutation(int $dado, array $tabela, int $quantidade): int
    {
        $dadoP = 0;

        for ($i = 0; $i < $quantidade; $i++) {
            $dadoP |= (($dado >> ($tabela[$i] - 1)) & (0x1)) << $i;
        }

        return $dadoP;
    }

    public static function reversePermutation($dado, $tabela, $quantidade): int
    {
        $dadoP = 0;

        for ($i = 0; $i < $quantidade; $i++) {
            $dadoP |= (($dado >> $i) & (0x1)) << ($tabela[$i] - 1);
        }

        return $dadoP;
    }

    public static function generateSubKey($chave, $quantity): int
    {
        $chaveEsq = ($chave >> 12) & 0xFFF;
        $chaveDir = $chave & 0xFFF;

        for ($i = 0; $i <= $quantity; $i++) {
            //rotações
            $chaveEsq = (($chaveEsq << self::rotL[$i]) + ($chaveEsq >> (12 - self::rotL[$i]))) & 0xFFF;
            $chaveDir = (($chaveDir << self::rotR[$i]) + ($chaveDir >> (12 - self::rotR[$i]))) & 0xFFF;
        }

        return self::permutation(($chaveEsq << 12) + $chaveDir, self::tbPC2, 24);
    }

    public static function sBox($dado): int
    {
        $dadoC = 0;

        $dadoAux = (($dado >> 18) & 0x1) + (($dado >> 22) & 0x2);
        $dadoC += (self::tbSBox0[$dadoAux][($dado >> 19) & (0xF)]) << 12;

        $dadoAux = (($dado >> 12) & 0x1) + (($dado >> 16) & 0x2);
        $dadoC += (self::tbSBox1[$dadoAux][($dado >> 13) & (0xF)]) << 8;

        $dadoAux = (($dado >> 6) & 0x1) + (($dado >> 10) & 0x2);
        $dadoC += (self::tbSBox2[$dadoAux][($dado >> 7) & (0xF)]) << 4;

        $dadoAux = (($dado >> 0) & 0x1) + (($dado >> 4) & 0x2);
        $dadoC += (self::tbSBox3[$dadoAux][($dado >> 1) & (0xF)]) << 0;

        return $dadoC;
    }
}