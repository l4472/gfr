<?php

require __DIR__ . '/../vendor/autoload.php';

use Logymetria\Gfr\Lock;
use Logymetria\Gfr\Process;

$clientKey = 12345;

$lock = new Lock(123456, 7);

$randomPassword = 2133909385; // Operator: 123400

$process = new Process($lock, $clientKey);

$openingCode = $process->openingCode($randomPassword);

$closingCode = $process->closingCode($openingCode);

$detachPassword = 279277373;

$attachCode = $process->generateAttachCode();

$detachCode = $process->generateDetachCode($detachPassword);

echo 'Código de Abertura: ' . $openingCode . PHP_EOL;
echo 'Código de Fechamento: ' . $closingCode . PHP_EOL;
echo 'Código de Associação: ' . $attachCode . PHP_EOL;
echo 'Código de Desassociação: ' . $detachCode . PHP_EOL;